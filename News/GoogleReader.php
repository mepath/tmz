<?php
	require_once('lib/phpQuery-onefile.php');
	class GoogleReader {
		static function scrape(){
			if(file_exists('news')) {
				//default data file
				$html = file_get_contents('news');
			} else {
				$url = 'http://www.google.com/news';
				$html = file_get_contents($url);
			}
			phpQuery::newDocumentHTML($html);

			foreach(pq('.top-stories-section h2 span.titletext') as $pq) {
				echo pq($pq)->html() . "\n";
			}
			phpQuery::unloadDocuments();
		}
	}

	GoogleReader::scrape();
?>
