<?php
	if(isset($_SERVER['argv'][1])) {
		$input = $_SERVER['argv'][1];
	} else {
		//default
		$input = 'tha';
	}

	class Permuter {
		private $arr_permutations;

		public function __construct($input) {
			$this->input = $input;
			$this->permute($input, 0, strlen($input));
		}

		/**
		 * permute 
		 * 
		 * generates and stores all n! permutations of $str
		 * 
		 * @param string $str string to permutate
		 * @param int $a starting position
		 * @param $n length of string
		 * @access private
		 * @return void
		 */
		private function permute($str, $a, $n){
			if ($a == $n)
				$this->arr_permutations[] = $str;
			else {
				for ($b = $a; $b < $n; $b++) {
					$this->swap($str,$a,$b); //swap characters
					$this->permute($str, $a+1, $n);
					$this->swap($str,$a,$b); // unswap characters
				}
			}
		}

		/**
		 * swap 
		 * 
		 * swaps position of character at position a and position b
		 * 
		 * @param string $str source string
		 * @param int $a position of one character
		 * @param int $b position of another character
		 * @access private
		 * @return void
		 */
		private function swap(&$str, $a, $b) {
				$temp = $str[$a];
				$str[$a] = $str[$b];
				$str[$b] = $temp;
		}	 

		/**
		 * display 
		 * 
		 * Displays the stored permutations
		 * 
		 * @access public
		 * @return void
		 */
		public function display() {
			printf('Generating %d permutations of "%s"...' . "\n", sizeof($this->arr_permutations), $this->input);
			for($i = 0; $i < sizeof($this->arr_permutations); ++$i) {
				echo ($i + 1) . ": {$this->arr_permutations[$i]}\n";
			}
		}
	}

	$permuter = new Permuter($input);
	$permuter->display();
?>
