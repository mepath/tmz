<?php
	require_once('autoload.php');

	use Congow\Orient\Graph;
	use Congow\Orient\Graph\Vertex;
	use Congow\Orient\Algorithm;

	//defaults
	$start_city = 'toronto';
	$end_city = 'dallas';

	if(isset($_SERVER['argv'][1])) {
		$start_city = strtolower($_SERVER['argv'][1]);
	}
	if(isset($_SERVER['argv'][2])) {
		$end_city = strtolower($_SERVER['argv'][2]);
	}

	$fh = fopen('file.txt', 'r');

	$arr_info = array();
	$arr_vertexes = array();

	while($row = fgetcsv($fh)) {
		$start = strtolower(trim($row[0]));
		$end = strtolower(trim($row[1]));
		$distance = trim($row[2]);
		$obj = new stdClass();
		$obj->start = $start;
		$obj->end = $end;
		$obj->distance = $distance;

		$arr_info[] = $obj;
		$arr_vertexes[$start] = (isset($arr_vertexes[$start]))? $arr_vertexes[$start] : new Vertex($start);
	}

	$graph = new Graph();
	foreach($arr_vertexes as $city => $vertex) {
		foreach($arr_info as $obj) {
			if($obj->start == $city) {
				if(isset($arr_vertexes[$obj->end])) {
					$vertex->connect($arr_vertexes[$obj->end], $obj->distance);
				}
			}
		}
		$graph->add($vertex);
	}

	if(in_array($start_city, array_keys($arr_vertexes)) && in_array($end_city, array_keys($arr_vertexes))) {
		$algorithm = new Algorithm\Dijkstra($graph);
		$algorithm->setStartingVertex($arr_vertexes[$start_city]);
		$algorithm->setEndingVertex($arr_vertexes[$end_city]);
		echo $algorithm->getLiteralShortestPath(). ": distance " . $algorithm->getDistance() . "\n";
	} else {
		echo "Path of $start_city to $end_city isn't supported\n";
	}
?>
